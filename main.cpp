#include <Eigen/Core>
#include <iostream>
#include <vector>
#include <ctime>

Eigen::MatrixXd func(
	const Eigen::MatrixXd &m,
	const int numDataSub
)
{
	int dimData = m.rows();
	Eigen::MatrixXd mSub = Eigen::MatrixXd::Zero(dimData, numDataSub);
	Eigen::VectorXd mNorm = m.colwise().norm();
	std::vector<int> indexMax(numDataSub);
	for(int n = 0; n < numDataSub; ++n)
	{
		mNorm.maxCoeff(&indexMax[n]);
		mNorm(indexMax[n]) = 0.0;
		mSub.col(n) = m.col(indexMax[n]);
	}
	return mSub;
}

int main()
{
	std::clock_t timeStart;
	std::clock_t timeEnd;
	int numTrial = 100;

	int dimData = 100;
	int numData = 100000;
	int numDataSub = 50;
	Eigen::MatrixXd m;
	Eigen::MatrixXd mSub = Eigen::MatrixXd::Zero(dimData, numDataSub);
	Eigen::VectorXd mNorm;
	std::vector<int> indexMax(numDataSub);

	// std::cout << "All data and its norm:" << std::endl;
	// for(int n = 0; n < numData; ++n)
	// {
	// 	std::cout << "	data[" << n << "] = (" << m.col(n).transpose() << ")	norm = " << mNorm(n) << std::endl;		
	// }

	// retrieve columns whose norm  is top numDataSub

	///
	// Method 1: column selection without using a function
	timeStart = std::clock();
	for(int t = 0; t < numTrial; ++t)
	{
		m = Eigen::MatrixXd::Random(dimData, numData);
		mNorm = m.colwise().norm();
		for(int n = 0; n < numDataSub; ++n)
		{
			mNorm.maxCoeff(&indexMax[n]);
			mNorm(indexMax[n]) = 0.0;
			mSub.col(n) = m.col(indexMax[n]);
		}
		// std::cout << "Selected data with " << numDataSub << "largest norm:" << std::endl;
		// for(int n = 0; n < numDataSub; ++n)
		// {
		// 	std::cout << "	data[" << n << "] = (" << mSub.col(n).transpose() << ")	original index = " << indexMax[n] << std::endl;		
		// }
	}
	timeEnd = std::clock();
	std::cout << "Elapsed time w/o using function = " << (double)(timeEnd - timeStart)/CLOCKS_PER_SEC << " sec. for " << numTrial << " trial" << std::endl;

	///
	// Method 2: column selection using a function
	timeStart = std::clock();
	for(int t = 0; t < numTrial; ++t)
	{
		m = Eigen::MatrixXd::Random(dimData, numData);
		mSub = func(m, numDataSub);
	}
	timeEnd = std::clock();
	std::cout << "Elapsed time w/  using function = " << (double)(timeEnd - timeStart)/CLOCKS_PER_SEC << " sec. for " << numTrial << " trial" << std::endl;

	return 0;
}