Eigen-select-columns
====================

This code selects columns with N largest norm from Eigen matrix.

Required libraries:
- CMake 2.8.12.2
- Eigen 3.2.2
Note that the project may not work with different versions of the libraries.

The project contains
- a single c++ code, main.cpp
- cmake related files, CMakeLists.txt

To run the code,
- $ cd build
- $ cmake ..
- $ make
- $ ./selectColumns
